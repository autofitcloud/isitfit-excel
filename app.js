/*
 * Copyright (c) Microsoft Corporation. All rights reserved. Licensed under the MIT license.
 * See LICENSE in the project root for license information.
 */

'use strict';

(function () {

	Office.onReady()
    .then(function() {
        $(document).ready(function () {

            // TODO1: Determine if the user's version of Office supports all the
            //        Office.js APIs that are used in the tutorial.
            if (!Office.context.requirements.isSetSupported('ExcelApi', '1.7')) {
                console.log('Sorry. The tutorial add-in uses Excel.js APIs that are not available in your version of Office.');
            }
            
            $("#dbgDiv").html("Ready");
            console.log("entered document.ready");

            // TODO2: Assign event handlers and other initialization logic.
            $('#create-table').click(createTable);

        });
    });

    // TODO3: Add handlers and business logic functions here.
    function createTable() {
        // $("#dbgDiv").html("pre-Calculating");

        Excel.run(function (context) {
            $("#dbgDiv").html("Calculating");
            
            // common
            var currentWorksheet = context.workbook.worksheets.getActiveWorksheet();
            
            // isitfit table
            var isitfitTable = currentWorksheet.tables.add("A1:B1", true); // parameter 2: hasHeaders
            isitfitTable.getHeaderRowRange().values = [["field", "value"]];
                    
            // get ec2 client and provided use aws credentials
            // https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/EC2.html
            var credentials = {
                accessKeyId: $('#aws_access_key_id').val(),
                secretAccessKey: $('#aws_secret_access_key').val(),
                region: $('#region').val()
            };
            // console.log("aws credentials = " + JSON.stringify(credentials));

            // test run isitfit
            return isitfit.main(credentials)
                .then(function(data) {
                    console.log("Results to isitfit table");
                    isitfitTable.rows.add(
                            null, // add at the end
                            [   ["Start date", data.priorDate],
                                ["End date", data.today],
                                ["Number of EC2 machines", data.n_ec2],
                                ["Total billed ($/hr)", data.total_billed],
                                ["Total used ($/hr)", data.total_used],
                                ["used/billed*100", data.total_used/data.total_billed*100]
                            ]
                        );
                    $("#dbgDiv").html("Ready");
                    return context.sync();
                })
                .catch(function(err) {
                    console.error("Error in isitfit.main: "+err);
                    $("#dbgDiv").html("Error: "+err);
                    
                    return context.sync();
                });

/*
            // prepare to add instance IDs in the sheet
            var iidTable = currentWorksheet.tables.add("G1:H1", true); // parameter 2: hasHeaders
            iidTable.name = "IidTable";
            iidTable.getHeaderRowRange().values = [["Instance ID", "Instance Type"]];

            // proceed
            var ec2 = new AWS.EC2(credentials);

            // list ec2 instances
            var params = {DryRun: false};
            var out_all = [];
            
            // return a promise as required by excel.run
            return ec2.describeInstances(params)
                .promise()
                .then(function(data) {
                
                    // console.log(data);
                    data.Reservations.forEach(function(r) {
                        r.Instances.forEach(function(x) {
                            console.log([x.InstanceId, x.InstanceType]);
                            out_all.push([x.InstanceId, x.InstanceType]);
                        });
                    });
                    
                    iidTable.rows.add(null, out_all);
                    
                    //----------------------
        
                    // TODO4: Queue table creation logic here.
                    var expensesTable = currentWorksheet.tables.add("A1:C1", true); // parameter 2: hasHeaders
                    expensesTable.name = "ExpensesTable";
            
                    // TODO5: Queue commands to populate the table with data.
                    expensesTable.getHeaderRowRange().values =
                        [["Date", "Merchant", "Amount"]];
                    
                    expensesTable.rows.add(null, [
                        ["1/1/2017", "Communications", "120"],
                        ["1/2/2017", "Transportation", "142.33"],
                        ["1/5/2017", "Groceries", "27.9"],
                        ["1/10/2017", "Restaurant", "33"],
                        ["1/11/2017", "Education", "350.1"],
                        ["1/15/2017", "Other", "135"],
                        ["1/15/2017", "Groceries", "97.88"]
                    ]);
            
                    // TODO6: Queue commands to format the table.
                    expensesTable.columns.getItemAt(2).getRange().numberFormat = [['€#,##0.00']];
                    expensesTable.getRange().format.autofitColumns();
                    expensesTable.getRange().format.autofitRows();
            
                    return context.sync();
                })
                .catch(function(err) {
                    console.error("Error in ec2.describeInstances: "+err);
                    $("#dbgDiv").html("Error in ec2.describeInstances: "+err);
                    
                    return context.sync();
                });
                */

        })
        .catch(function (error) {
            console.log("Error: " + error);
            if (error instanceof OfficeExtension.Error) {
                console.log("Debug info: " + JSON.stringify(error.debugInfo));
            }
        });
    }

})();
