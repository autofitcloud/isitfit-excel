#!/bin/sh
# Build steps required for isitfit-excel

set -e
set -x

# install dependencies
npm install

# browserify isitfit-nodejs
node_modules/browserify/bin/cmd.js \
  node_modules/isitfit/src/L0Manager.js \
  --standalone isitfit \
  -o node_modules/isitfit/dist/isitfit.js

# generate bundle.js
npm run-script build

# copy files
cp -r \
   app.css \
   bundle.js \
   node_modules/core-js/client/core.js \
   node_modules/isitfit/dist/isitfit.js \
   index.html \
   assets/ \
   public/
