# isitfit-excel

This is a port of [isitfit](https://gitlab.com/autofitcloud/isitfit) (originally written in python) as an excel add-in.


## Dev notes

This repo was based on
- https://github.com/OfficeDev/Excel-Add-in-Tutorial
- https://docs.microsoft.com/en-us/office/dev/add-ins/tutorials/excel-tutorial

Dev usage

```
# build isitfit-nodejs into a browser importable file
# Note 1: this was *not* committed into the repo because it's 6MB (compared to the repo itself being 0.5MB)
# Note 2: need --standalone to expose the module to the client javscript
node_modules/browserify/bin/cmd.js node_modules/isitfit/src/L0Manager.js --standalone isitfit -o node_modules/isitfit/dist/isitfit.js

npm install
npm run-script build
npm start

# needed if on aws machine with blocked ports
node_modules/localtunnel/bin/client -p 3000
# or
ngrok http 3000 --subdomain=isitfit-excel
# or with browser-sync (i.e. without npm start)
ngrok http file://$PWD --subdomain=isitfit-excel
```

Added `isitfit-nodejs` with

```
npm install --save git+https://git@gitlab.com/autofitcloud/isitfit-nodejs.git#0.1.0
npm install --save git+https://git@gitlab.com/autofitcloud/isitfit-nodejs.git#master
```

Whenever an update is made to the `app.js` file, a `npm run-script build` is needed as well as a `load taskpane` in web office excel.

Also note that saving info into the hml is not a good idea with browser-sync because it will just reload the page.
Browse-sync will not even call `office.onready` again.

Other excel add-in projects: https://github.com/nuitsjp/CopyToMarkdownAddIn


## Published manifest

The URL in the manifest should be either

- https://isitfit-excel.ngrok.io for the dev version
- https://isitfit-excel-addin-url.netlify.com
