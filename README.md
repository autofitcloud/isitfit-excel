# isitfit-excel

Calculate AWS EC2 waste in Excel.


## Dev notes

Check file `DEVELOPER.md`


## License

Apache license 2.0. Check file `LICENSE`


## Privacy

Check https://autofitcloud.com/privacy
