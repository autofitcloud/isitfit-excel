Version latest (1.0.1.0?, 2019-09-06?)

- enh: deploy with netlify
- enh: deployable files now gathered into `public` folder
- enh: replace generic icons


Version 1.0.0.0 (2019-09-08)

- enh: version bump to match required format and minimum for https://sellerdashbaord.microsoft.com


Version 0.1.0 (2019-09-06)

- FEAT: split out isitfit-nodejs and add it as an external dependency
- feat: add babel dependencies for async
    - based on https://stackoverflow.com/a/40880340/4126114
    - and https://stackoverflow.com/a/53712824/4126114
